const mongoose = require('mongoose');
const options = {
  useNewUrlParser: true,
  socketTimeoutMS: 30000,
  keepAlive: true,
  reconnectTries: 30000,
  useCreateIndex: true
};

mongoose.connect('mongodb://localhost/voteDB', options
).then(() => {
  console.log('MongoDB connect has started');
}).catch((e) => {
  console.log(e);
});
