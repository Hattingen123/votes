const mongoose = require('mongoose');
const connect = require('./index');

const voteOptionsSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  count: Number
});

const voteSchema = new mongoose.Schema({
  name: {
  	type: String,
  	required: true
  },
  desc: String,
  voteOptions: {
    type: [voteOptionsSchema],
    required: true
  },
  files: [],
  isClosed: {
  	type: Boolean,
  	default: false
  },
  votedIPs: [],
  createdAt: String
});

const Vote = mongoose.model('Vote', voteSchema, 'votes');

exports.getAllVotes = function () {
  console.log('called getAllVotes');

  return Vote.find().sort('-createdAt').then((res) => {
  	return res;
  }).catch((e) => {
  	return e;
  });
};

exports.addVote = function (body) {
  console.log('called addVote');

  var newVote = new Vote({name: body.name, desc: body.desc, voteOptions: body.voteOptions, createdAt: new Date()});
  return newVote.save().then((res) => {
  	return newVote;
  });
};

exports.getVote = function (id) {
  console.log('called getVote');

  return Vote.find({_id: id}).then((res) => {
  	return res;
  });
};

exports.chooseOption = function (id, optionIndex, ip) {
  console.log('called chooseOption');

  return Vote.findOne({_id: id}).then((vote) => {
    if (vote.votedIPs.includes(ip)) {
      return 'IP already voted';

    } else {

      for (var opt = 0; opt < vote.voteOptions.length; opt++) {

      	if ((vote.voteOptions[optionIndex].name  === vote.voteOptions[opt].name) && vote.isClosed !== true) {
      	  vote.voteOptions[opt].count += 1;
          vote.votedIPs.push(ip);
      	  vote.markModified('voteOptions');

      	  return vote.save().then((res) => {
      	  	return res;
      	  }).catch((e) => {
      	  	return e;
      	  });
      	} else if ((vote.voteOptions[optionIndex].name  === vote.voteOptions[opt].name) && vote.isClosed === true) {
          return "Vote is already closed";
        };
      };
    };
  });
};

exports.closeVote = function (id) {
  console.log('called closeVote');

  return Vote.findOne({_id: id}).then((vote) => {
  	vote.isClosed = true;
  	return vote.save().then((res) => {
  	  return res;
  	}).catch((e) => {
  	  return e;
  	});
  });
};

exports.reopenVote = function (id) {
  console.log('called reopenVote');

  return Vote.findOne({_id: id}).then((vote) => {
  	vote.isClosed = false;
  	return vote.save().then((res) => {
  	  return res;
  	}).catch((e) => {
  	  return e;
  	});
  });
};
