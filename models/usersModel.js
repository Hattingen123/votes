const mongoose = require('mongoose');
const connect = require('./index');

const userSchema = new mongoose.Schema({
  email: {
  	type: String,
  	required: true,
  	unique: true
  },
  password: {
  	type: String,
  	required: true
  }
});

const User = mongoose.model('User', userSchema, 'users');

exports.addUser = function (email, password) {
  console.log('called addUser');

  var newUser = new User({email: email, password: password})
  return newUser.save();
};

exports.getUserByEmail = function(email) {
  return User.findOne({email: email});
};

exports.getUserById = function(id) {
  return User.findOne({_id: id});
};