const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const mongoose = require('mongoose');

const userModel = require('../models/usersModel.js');


// configure passport.js to use the local strategy
passport.use(new LocalStrategy(
  { usernameField: 'email' },
  (email, password, done) => {

    const user = userModel.getUserByEmail(email).then((user) => {

      if (!user) {
      	return done(null, false, { message: 'User not found' })
      }

      if (password !== user.password) {
        return done(null, false, {message: 'Incorrect password'});
      }

      return done(null, user);
      

    }).catch((e) => {
      return done(null, false, {message: 'Error on DB request'});
  	});
  }
));

// tell passport how to serialize/deserialize the user
passport.serializeUser((user, done) => {

  if (user) {done(null, user._id)};
});

passport.deserializeUser((id, done) => {

  userModel.getUserById(mongoose.Types.ObjectId(id)).then((res) => {
  	const user = JSON.stringify(res._id) === JSON.stringify(id) ? res : false;
  	done(null, user);
  }).catch((e) => {
  	done(e);
  });
});

module.exports = passport;
