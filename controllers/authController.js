const usersModel = require('../models/usersModel');

const passport = require('../auth/passport');

exports.login = passport.authenticate('local', 
  { successRedirect: '/', failureRedirect: '/login', failureFlash: true });

exports.getLoginPage = function (req, res) {
  const error = req.flash('error');
  res.render('login', { isAuthenticated: req.isAuthenticated(), user: req.user, error: error });
  res.status(200);
};

exports.logout = function (req, res) {
  req.logOut();
  res.redirect('/');
};

exports.addNewUser = function (req, res, next) {
  const newUser = usersModel.addUser(req.body.email, req.body.password).then((user) => {
    next();
  }).catch((e) => {
    req.flash('error', 'User already exist');
    res.redirect('/login');
  });
};

exports.isAuthMiddleware = function (req, res, next) {
  if(!req.isAuthenticated()) {
    res.redirect('/');
    return;
  }

  next();
};