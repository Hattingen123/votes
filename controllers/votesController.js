const votesModel = require('../models/votesModel');

exports.getAllVotes = function (req, res) {
  res.status(500);

  votesModel.getAllVotes().then((votes) => {
  	res.status(200);
  	res.send(JSON.stringify(votes));
  }).catch((e) => {
  	res.send(e.message);
  });
};

exports.getAllVotesForIndex = function (req, res) {
  const votes = votesModel.getAllVotes().then((votes) => {
  	res.render('index', { votes: votes, ip: req.ip, isAuthenticated: req.isAuthenticated(), user: req.user });
  	res.status(200);
  }).catch((e) => {
    res.send(e.message);
  });
};

exports.addVote = function (req, res) {
  res.status(500);

  votesModel.addVote(req.body).then((newVote) => {
    res.status(201);
    res.send(JSON.stringify(newVote));
  }).catch((e) => {
    const errors = {};

    if (e.errors.name) {
      errors.name = {
        field: 'name', 
        message: 'Name is required'
      };
    }

    for (var i = 0 ; i < req.body.voteOptions.length; i++) {
      const errorString = 'voteOptions.' + i + '.name';
      if (e.errors[errorString]) {
        errors[errorString] = {
          field: errorString,
          message: 'Options' + i + ' are required'
        };
      }
    }

    res.send(JSON.stringify(errors));
  });
};

exports.getVote = function (req, res) {
  res.status(500);

  votesModel.getVote(req.params.id).then((vote) => {
  	res.status(200);
  	res.send(JSON.stringify(vote));
  }).catch((e) => {
  	res.send(e.message);
  });
};

exports.chooseOption = function (req, res) {
  res.status(500);

  votesModel.chooseOption(req.params.id, req.params.optionIndex, req.ip).then((updatedVote) => {
  	res.status(200);
  	res.send(JSON.stringify(updatedVote));
  }).catch((e) => {
  	res.send(e.message);
  });
};

exports.closeVote = function (req, res) {
  res.status(500);

  votesModel.closeVote(req.params.id).then((closedVote) => {
  	res.status(200);
    // ???
  	res.send(JSON.stringify(closedVote));
  }).catch((e) => {
  	res.send(e.message);
  });
};

exports.reopenVote = function (req, res) {
  res.status(500);

  votesModel.reopenVote(req.params.id).then((reopenedVote) => {
  	res.status(200);
  	res.send(JSON.stringify(reopenedVote));
  }).catch((e) => {
  	res.send(e.message);
  });
};
