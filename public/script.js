document.addEventListener("DOMContentLoaded", function() {

  const URL = "http://192.168.131.45:8080";
  //const URL = "http://localhost:8080"


// construct newVote and POST it
  const addVoteForm = document.getElementById('addVoteForm');

  addVoteForm.onsubmit = function (event) {
  	event.preventDefault();

  	const xhttp = new XMLHttpRequest();

  	const optionsContainer = document.getElementById('voteOptions');
  	const options = optionsContainer.getElementsByClassName('form-control');

  	var voteOptions = [];

  	for (var i = 0; i < options.length; i++) {
  	  if ( options[i].value !== '') {
  	    voteOptions[i] = {
  	  	  name: options[i].value,
  	  	  count: 0
  	    };
  	  } else {
        voteOptions[i] = {};
      };
  	};

  	const newVote = {
  	  name: document.getElementById('inputName').value,
  	  desc: document.getElementById('inputDesc').value,
  	  voteOptions: voteOptions
  	};

  	xhttp.open("POST", URL + '/votes/', true);
  	xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	  xhttp.setRequestHeader("Content-type", "application/json");
	  xhttp.setRequestHeader("DataType", "json");

  	xhttp.send(JSON.stringify(newVote));

  	xhttp.onreadystatechange = function () {

  	  if (xhttp.readyState !== 4) return;

  	  document.getElementById('errorDiv').innerHTML = '';

  	  if (xhttp.status === 201) {
  		  location = location;
  	  } else {

  	  	var res = JSON.parse(xhttp.response);
  	  	var keys = Object.keys(res);

  	  	for (var i = 0; i < keys.length; i++ ) {
  	  	  var error = res[keys[i]].message;
  	  	  document.getElementById('errorDiv').innerHTML += '<p>' + error + '</p>';
  	  	};

  	  	if (res.name) {
  	  	  document.getElementById('inputName').parentNode.setAttribute("class", "form-group has-error");
  	  	} else {
  	  	  document.getElementById('inputName').parentNode.setAttribute("class", "form-group");
  	  	};

        for (var i = 0 ; i < newVote.voteOptions.length ; i++) {
          const errorString = 'voteOptions.' + i + '.name';

          if (res[errorString]) {
            document.getElementById('voteOptions').setAttribute("class", "form-group has-error");
          } else {
            document.getElementById('voteOptions').setAttribute("class", "form-group");
          }
        }

  	  	
  	  };
  	};
  };

// add more optionInputs to form
  const moreOptionsButton = document.getElementById('more_options');

  moreOptionsButton.onclick = function () {
  	const optionsContainer = document.getElementById('voteOptions');
  	const newOption = document.createElement('input');

  	newOption.setAttribute("type", "text");
  	newOption.setAttribute("class", "form-control");
  	newOption.setAttribute("placeholder", "Option");

  	optionsContainer.appendChild(newOption);
  };

// remove optionInputs from form
  const lessOptionsButton = document.getElementById('less_options');

  lessOptionsButton.onclick = function () {
  	const optionsContainer = document.getElementById('voteOptions');
  	const optionsFields = optionsContainer.getElementsByClassName('form-control');

  	optionsFields[ optionsFields.length - 1 ].parentNode.removeChild(optionsFields[ optionsFields.length - 1 ]);
  };

// hide and show addVoteForm

  const hideFormButton = document.getElementById('hide-form');
  var hiddenState = true;

  hideFormButton.onclick = function () {
  	const addVoteForm = document.getElementById('addVoteForm');

  	if (hiddenState) {
  	  addVoteForm.setAttribute("class", "show");
  	  hideFormButton.innerHTML = "Hide";
  	} else {
  	  addVoteForm.setAttribute("class", "hidden");
  	  hideFormButton.innerHTML = "Add new Vote";
  	};

  	hiddenState = !hiddenState;
  };

//get Vote details for every Vote

  // const voteTableRows = document.getElementById('votes-tbody').getElementsByTagName('tr');

  // for (var row = 0; row < voteTableRows.length; row++) {
  // 	voteTableRows[row].onclick = function () {
	 //  const xhttp = new XMLHttpRequest();
	 //  const req_url = URL + '/votes/' + this.getElementsByTagName('td')[0].getAttribute('id');

	 //  xhttp.open("GET", req_url, true);
	 //  xhttp.send();

	 //  xhttp.onreadystatechange = function () {
	 //  	if (xhttp.readyState === 4) {
	 //  	  const vote = JSON.parse(xhttp.response);
	 //  	  console.log(vote); 
	 //  	};
	 //  };
  // 	};
  // };

//choose option for a Vote

  const chooseOptionButtons = document.getElementById('votes-tbody').getElementsByClassName('voteButton');

  for (var btn = 0; btn < chooseOptionButtons.length; btn++) {

  	chooseOptionButtons[btn].onclick = function () {

  	  for (var radioIndex = 0; radioIndex < this.parentNode.getElementsByTagName('input').length; radioIndex++) {
  	  	if (this.parentNode.getElementsByTagName('input')[radioIndex].checked) {
  	  	  const xhttp = new XMLHttpRequest();

  	  	  const req_url = URL + '/votes/' 
	  		+ this.parentNode.getAttribute('id')
	  		+ '/choose/'
	  		+ radioIndex;

	  	  xhttp.open("GET", req_url, true);
	  	  xhttp.send();

	  	  xhttp.onreadystatechange = function () {
	  	    if (xhttp.readyState === 4) {
	  	      const vote = JSON.parse(xhttp.response);	      
	  	      location = location;
	  	    };
	      };
  	    };
  	  };
  	};
  };

//close Vote

  const closeVoteButtons = document.getElementById('votes-tbody').getElementsByClassName('closeVoteButton');

  for (var btn = 0; btn < closeVoteButtons.length; btn++) {
  	closeVoteButtons[btn].onclick = function () {
  	  const xhttp = new XMLHttpRequest();
  	  const req_url = URL + '/votes/'
  	  	+ this.parentNode.getAttribute('id')
  	  	+ '/close/';

  	  xhttp.open("GET", req_url, true);
	  xhttp.send();

	  xhttp.onreadystatechange = function () {
  	    if (xhttp.readyState === 4) {
  	      const vote = JSON.parse(xhttp.response);
  	      location = location;
  	    };
      };
  	};
  };

//reopen Vote

  const reopenVoteButtons = document.getElementById('votes-tbody').getElementsByClassName('reopenVoteButton');

  for (var btn = 0; btn < reopenVoteButtons.length; btn++) {
  	reopenVoteButtons[btn].onclick = function () {
  	  const xhttp = new XMLHttpRequest();
  	  const req_url = URL + '/votes/'
  	  	+ this.parentNode.getAttribute('id')
  	  	+ '/reopen/';

  	  xhttp.open("GET", req_url, true);
	    xhttp.send();

	    xhttp.onreadystatechange = function () {
  	    if (xhttp.readyState === 4) {
  	      const vote = JSON.parse(xhttp.response);
  	      location = location;
  	    };
      };
  	};
  };

//document.ready finishes
});
