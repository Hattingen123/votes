const express = require('express');
const router = express.Router();

const votesController = require('./controllers/votesController');
const authController = require('./controllers/authController');
const votesModel = require('./models/votesModel');
const userModel = require('./models/usersModel');

const passport = require('./auth/passport');

// Main page with all Votes data
router.get('/',
  votesController.getAllVotesForIndex
);

// login, logout and register routes
router.post('/login',
  authController.login
);

router.get('/login',
  authController.getLoginPage
);

router.get('/logout', 
  authController.logout
);

router.post('/register',
  authController.addNewUser,
  authController.login
);

// router.get('/authrequired', (req, res) => {
//   if(req.isAuthenticated()) {
//     res.send('you hit the authentication endpoint\n');
//   } else {
//     res.redirect('/');
//   }
// });

//votes routes
router.get('/votes',
  votesController.getAllVotes
);

router.post('/votes',
  authController.isAuthMiddleware,
  votesController.addVote
);

router.get('/votes/:id',
  votesController.getVote
);

router.get('/votes/:id/choose/:optionIndex',
  votesController.chooseOption
);

router.get('/votes/:id/close',
  authController.isAuthMiddleware,
  votesController.closeVote
);

router.get('/votes/:id/reopen',
  authController.isAuthMiddleware,
  votesController.reopenVote
);

module.exports = router;
