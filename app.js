const express = require('express');
const ejs = require('ejs');
const uuid = require('uuid/v4');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const bodyParser = require('body-parser');
const flash = require('connect-flash');

const passport = require('./auth/passport.js');
const router = require('./router.js');

const app = express();

app.use(session({
  genid: (req) => {
    return uuid(); // use UUIDs for session IDs
  },
  store: new MongoStore({
  	url: 'mongodb://localhost:27017/sessions'
  }),
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}));

app.use(bodyParser.urlencoded({ extended: true }));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use(express.static('public'));
app.use(express.json());
app.use('/', router);

app.engine('html', ejs.renderFile);
app.set('view engine', 'html');

app.listen(8080, '0.0.0.0', () => console.log('Example app listening on port 8080!'));
